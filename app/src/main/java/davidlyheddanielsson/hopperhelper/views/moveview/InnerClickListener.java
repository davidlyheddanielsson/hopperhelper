package davidlyheddanielsson.hopperhelper.views.moveview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class InnerClickListener implements RecyclerView.OnItemTouchListener
{
    private OnMoveClickListener listener;

    public interface OnMoveClickListener
    {
        public void onItemClick(View view, int position);
        public void onLongItemClick(View view, int position);
    }

    private GestureDetector gestureDetector;

    public InnerClickListener(Context context, final RecyclerView moveView, final OnMoveClickListener listener)
    {
        this.listener = listener;

        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener()
        {
            @Override
            public boolean onSingleTapUp(MotionEvent e)
            {
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e)
            {
                View child = moveView.findChildViewUnder(e.getX(), e.getY());
                if(child != null && listener != null)
                    listener.onLongItemClick(child, moveView.getChildAdapterPosition(child));
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent e)
    {
        View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
        if(childView != null && listener != null && gestureDetector.onTouchEvent(e))
        {
            listener.onItemClick(childView, recyclerView.getChildAdapterPosition(childView));
            return true;
        }

        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e)
    { }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
    { }
}