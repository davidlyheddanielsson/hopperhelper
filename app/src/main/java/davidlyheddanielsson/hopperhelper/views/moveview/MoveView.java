package davidlyheddanielsson.hopperhelper.views.moveview;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

import davidlyheddanielsson.hopperhelper.FileUtils;
import davidlyheddanielsson.hopperhelper.Move;
import davidlyheddanielsson.hopperhelper.R;
import davidlyheddanielsson.hopperhelper.views.stringlist.StringList;

public class MoveView extends RecyclerView
{
    public interface OnMoveClickListener
    {
        void OnMoveClick(View view, Move move);
        void OnMoveLongClick(View view, Move move);
    }

    Adapter adapter = new Adapter();
    OnMoveClickListener onMoveClickListener = null;

    Context context;

    public MoveView(Context context)
    {
        super(context);
        init(context);
    }

    public MoveView(Context context,
                    @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        init(context);
    }

    public MoveView(Context context, @Nullable AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init(context);
    }

    @Override
    public void addOnItemTouchListener(OnItemTouchListener listener)
    {
        throw new UnsupportedOperationException("not supported");
    }

    private void init(Context context)
    {
        this.context = context;

        setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
        addItemDecoration(new ItemDecoration());
        setAdapter(adapter);

        /*InnerClickListener.OnMoveClickListener listener = new InnerClickListener.OnMoveClickListener()
        {
            @Override
            public void onItemClick(View view,
                                    int position)
            {
                if(onMoveClickListener != null)
                    onMoveClickListener.OnMoveClick(view, getMoveAt(position));
            }

            @Override
            public void onLongItemClick(View view,
                                        int position)
            {
                if(onMoveClickListener != null)
                    onMoveClickListener.OnMoveLongClick(view, getMoveAt(position));
            }
        };
        super.addOnItemTouchListener(new InnerClickListener(context, this, listener));*/
    }

    public void setOnMoveClickListener(OnMoveClickListener onMoveClickListener)
    {
        this.onMoveClickListener = onMoveClickListener;
    }

    private Move getMoveAt(int position)
    {
        if(position < adapter.sixes.size())
            return adapter.sixes.get(position);

        position -= adapter.eights.size();
        if(position < adapter.eights.size())
            return adapter.eights.get(position);

        position -= adapter.charleston.size();
        return adapter.charleston.get(position);
    }

    public void setChildren(SparseArray<Move> children)
    {
        adapter.setChildren(children);
    }

    public void setSixes(ArrayList<Move> sixes)
    {
        adapter.setSixes(sixes);
        adapter.notifyDataSetChanged();
    }

    public void setEights(ArrayList<Move> eights)
    {
        adapter.setEights(eights);
        adapter.notifyDataSetChanged();
    }

    public void setCharleston(ArrayList<Move> charleston)
    {
        adapter.setCharleston(charleston);
        adapter.notifyDataSetChanged();
    }

    private class ViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout rootLayout;
        TextView title;
        LinearLayout children;

        Move move;

        private ViewHolder(View itemView)
        {
            super(itemView);

            rootLayout = (LinearLayout)itemView.findViewById(R.id.layout_move_tile_rootlayout);
            title = (TextView)itemView.findViewById(R.id.layout_move_tile_title);
            children = (LinearLayout)itemView.findViewById(R.id.layout_move_tile_children);
        }
    }

    private class Adapter extends RecyclerView.Adapter<ViewHolder>
    {
        private ArrayList<Move> sixes = new ArrayList<>();
        private ArrayList<Move> eights = new ArrayList<>();
        private ArrayList<Move> charleston = new ArrayList<>();
        private SparseArray<Move> children;

        public void setSixes(ArrayList<Move> sixes)
        {
            this.sixes = sixes;
        }

        public void setEights(ArrayList<Move> eights)
        {
            this.eights = eights;
        }

        public void setCharleston(ArrayList<Move> charleston)
        {
            this.charleston = charleston;
        }

        public void setChildren(SparseArray<Move> children)
        {
            this.children = children;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_move_tile, parent, false);
            return new ViewHolder(layoutView);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position)
        {
            int color;
            Move move;

            if(position < sixes.size())
            {
                move = sixes.get(position);
                color = R.color.colorSix;
            }
            else
            {
                position -= sixes.size();
                if(position < eights.size())
                {
                    move = eights.get(position);
                    color = R.color.colorEight;
                }
                else
                {
                    position -= eights.size();

                    move = charleston.get(position);
                    color = R.color.colorCharleston;
                }
            }

            holder.title.setText(move.getName());
            holder.title.setTag(move);
            holder.title.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(onMoveClickListener != null)
                        onMoveClickListener.OnMoveClick(v, (Move)v.getTag());
                }
            });
            holder.rootLayout.setBackgroundColor(context.getColor(color));
            holder.move = move;

            if(holder.children.getChildCount() > move.getChildren().size())
            {
                for(int i = 0; i < move.getChildren().size(); i++)
                    ((TextView)holder.children.getChildAt(i)).setText(children.get(move.getChildren().get(i)).getName());

                holder.children.removeViews(move.getChildren().size(), holder.children.getChildCount() - move.getChildren().size());
            }
            else
            {
                int i = 0;
                for(; i < holder.children.getChildCount(); ++i)
                    ((TextView)holder.children.getChildAt(i)).setText(children.get(move.getChildren().get(i)).getName());

                for(; i < move.getChildren().size(); ++i)
                {
                    TextView textView = new TextView(context);
                    textView.setText(children.get(move.getChildren().get(i)).getName());
                    holder.children.addView(textView);
                    textView.setTag(children.get(move.getChildren().get(i)));
                    textView.setOnClickListener(new OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            if(onMoveClickListener != null)
                                onMoveClickListener.OnMoveClick(v, (Move)v.getTag());
                        }
                    });
                }
            }

        }

        @Override
        public int getItemCount()
        {
            return sixes.size() + eights.size() + charleston.size();
        }
    }

    private class ItemDecoration extends RecyclerView.ItemDecoration
    {
        @Override
        public void getItemOffsets(Rect outRect,
                                   View view,
                                   RecyclerView parent,
                                   State state)
        {
            outRect.left = 5;
            outRect.right = 5;
            outRect.top = 5;
            outRect.bottom = 5;
        }
    }
}
