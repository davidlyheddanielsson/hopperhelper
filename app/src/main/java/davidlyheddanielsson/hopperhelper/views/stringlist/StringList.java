package davidlyheddanielsson.hopperhelper.views.stringlist;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import java.util.ArrayList;


public class StringList extends RecyclerView
{
    private StringListAdapter adapter = null;

    public interface OnItemClickListener
    {
        void onItemClick(String text);
    }

    public StringList(Context context)
    {
        super(context);
        init(context);
    }

    public StringList(Context context,
                      @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        init(context);
    }

    public StringList(Context context, @Nullable AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context)
    {
        setLayoutManager(new LinearLayoutManager(context));
        adapter = new StringListAdapter(context);
        setAdapter(adapter);
    }

    public void setOnItemClickListener(OnItemClickListener listener)
    {
        adapter.setOnItemClickListener(listener);
    }

    public void setStrings(ArrayList<String> strings)
    {
        adapter.setStrings(strings);
    }
}
