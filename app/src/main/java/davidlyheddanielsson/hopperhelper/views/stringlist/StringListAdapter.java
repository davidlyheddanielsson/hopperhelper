package davidlyheddanielsson.hopperhelper.views.stringlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;

import davidlyheddanielsson.hopperhelper.R;

public class StringListAdapter extends RecyclerView.Adapter<StringListAdapter.ViewHolder>
{
    private Context context;
    private StringList.OnItemClickListener onItemClickListener;
    private ArrayList<String> strings;

    public StringListAdapter(Context context)
    {
        this.context = context;
        this.strings = new ArrayList<>();
    }

    public void setStrings(ArrayList<String> strings)
    {
        this.strings = strings;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_list_string, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder,
                                 int position)
    {
        final int pos = position; // Java why

        holder.textView.setText(strings.get(position));
        holder.textView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onItemClickListener.onItemClick(strings.get(pos));
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return strings.size();
    }

    public void setOnItemClickListener(StringList.OnItemClickListener onItemClickListener)
    {
        this.onItemClickListener = onItemClickListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView textView;

        public ViewHolder(View itemView)
        {
            super(itemView);

            textView = (TextView)itemView.findViewById(R.id.adapter_list_string);
        }
    }
}
