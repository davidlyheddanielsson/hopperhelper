package davidlyheddanielsson.hopperhelper.views;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class TextViewAdapter extends BaseAdapter
{
    private final Context context;
    private ArrayList<String> texts;

    public TextViewAdapter(Context context, ArrayList<String> texts)
    {
        this.context = context;
        this.texts = texts;
    }

    @Override
    public int getCount()
    {
        return texts.size();
    }

    @Override
    public Object getItem(int position)
    {
        return null;
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        TextView textView;

        if(convertView == null)
        {
            textView = new TextView(context);

        }
        else
            textView = (TextView)convertView;

        textView.setText(texts.get(position));
        return textView;
    }
}
