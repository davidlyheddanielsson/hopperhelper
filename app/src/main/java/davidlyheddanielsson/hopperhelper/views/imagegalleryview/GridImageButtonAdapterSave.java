package davidlyheddanielsson.hopperhelper.views.imagegalleryview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;

import davidlyheddanielsson.hopperhelper.R;

public class GridImageButtonAdapterSave extends RecyclerView.Adapter<GridImageButtonAdapterSave.ViewHolder>
{
    public interface OnClickListener
    {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Bitmap> thumbnails = new ArrayList<>();
    private ArrayList<Bitmap> bitmaps = new ArrayList<>();
    private ArrayList<Boolean> saved = new ArrayList<>();
    private GridImageButtonAdapterSave.OnClickListener onClickListener;

    public GridImageButtonAdapterSave(Context context)
    {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void addBitmap(Bitmap thumbnail, Bitmap bitmap)
    {
        thumbnails.add(thumbnail);
        bitmaps.add(bitmap);
        saved.add(null);

        notifyDataSetChanged();
    }

    public int addLoading()
    {
        thumbnails.add(null);
        bitmaps.add(null);
        saved.add(null);

        notifyDataSetChanged();

        return thumbnails.size() - 1;
    }

    public void setBitmaps(int index, Bitmap thumbnail, Bitmap bitmap)
    {
        thumbnails.set(index, thumbnail);
        bitmaps.set(index, bitmap);
        saved.set(index, false);

        notifyDataSetChanged();
    }

    public void setSaved(int index)
    {
        saved.set(index, true);

        notifyDataSetChanged();
    }

    public boolean doneLoading()
    {
        for(Boolean bool : saved)
        {
            if(bool == null || !bool)
                return false;
        }

        return true;
    }

    public Bitmap getThumbnail(int position)
    {
        return thumbnails.get(position);
    }

    public Bitmap getBitmap(int position)
    {
        return bitmaps.get(position);
    }

    public void removeBitmap(int index)
    {
        thumbnails.remove(index);
        bitmaps.remove(index);

        notifyDataSetChanged();
    }

    public void setOnClickListener(GridImageButtonAdapterSave.OnClickListener listener)
    {
        this.onClickListener = listener;
    }

    @Override
    public GridImageButtonAdapterSave.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = inflater.inflate(R.layout.layout_imagebuttongrid_item, parent, false);
        return new GridImageButtonAdapterSave.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GridImageButtonAdapterSave.ViewHolder holder, int position)
    {
        Bitmap thumbnail = thumbnails.get(position);

        if(thumbnail != null)
        {
            holder.button.setImageBitmap(thumbnails.get(position));
            holder.button.setVisibility(View.VISIBLE);
            holder.button.setId(position);

            holder.progressBar.setVisibility(View.GONE);
        }
        else
        {
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.button.setVisibility(View.GONE);
        }

        if(saved.get(position) == null)
            holder.savedImage.setVisibility(View.GONE);
        else
        {
            holder.savedImage.setVisibility(View.VISIBLE);

            if(saved.get(position))
                holder.savedImage.setColorFilter(Color.rgb(0, 200, 0));
            else
                holder.savedImage.setColorFilter(Color.rgb(200, 0, 0));
        }
    }

    @Override
    public int getItemCount()
    {
        return thumbnails.size();
    }

    public ArrayList<Bitmap> getBitmaps()
    {
        return bitmaps;
    }

    public ArrayList<Bitmap> getThumbnails()
    {
        return thumbnails;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
    {
        SimpleDraweeView button;
        ImageView savedImage;
        ProgressBar progressBar;

        ViewHolder(View itemView)
        {
            super(itemView);

            this.button = (SimpleDraweeView)itemView.findViewById(R.id.layout_imagebuttongrid_button);
            this.savedImage = (ImageView)itemView.findViewById(R.id.layout_imagebuttongrid_savedImage);
            this.progressBar = (ProgressBar)itemView.findViewById(R.id.layout_imagebuttongrid_progressbar);

            this.button.setOnClickListener(this);
            this.button.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            if(onClickListener != null)
                onClickListener.onClick(v, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v)
        {
            if(onClickListener != null)
            {
                onClickListener.onLongClick(v, getAdapterPosition());
                return true;
            }

            return false;
        }
    }
}
