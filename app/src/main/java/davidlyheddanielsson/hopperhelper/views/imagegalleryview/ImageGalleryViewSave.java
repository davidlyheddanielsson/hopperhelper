package davidlyheddanielsson.hopperhelper.views.imagegalleryview;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;

public class ImageGalleryViewSave extends RecyclerView
{
    public interface OnImageRemovedListener
    {
        void onImageRemoved(int clickedImage);
    }

    private GridImageButtonAdapterSave imageGalleryAdapter;

    private int clickedImage;

    private Context context;

    private FragmentManager fragmentManager;

    private OnImageRemovedListener onImageRemovedListener;

    public void setFragmentManager(FragmentManager fragmentManager)
    {
        this.fragmentManager = fragmentManager;
    }

    public void setOnImageRemovedListener(OnImageRemovedListener onImageRemovedListener)
    {
        this.onImageRemovedListener = onImageRemovedListener;
    }

    DialogInterface.OnClickListener removeImageDialogListener = new DialogInterface.OnClickListener()
    {
        @Override
        public void onClick(DialogInterface dialog, int which)
        {
            switch(which)
            {
                case DialogInterface.BUTTON_POSITIVE:
                    imageGalleryAdapter.removeBitmap(clickedImage);

                    if(onImageRemovedListener != null)
                        onImageRemovedListener.onImageRemoved(clickedImage);
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }

            clickedImage = -1;
        }
    };

    public ImageGalleryViewSave(Context context)
    {
        super(context);
        this.context = context;

        init();
    }

    public ImageGalleryViewSave(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        this.context = context;

        init();
    }

    public ImageGalleryViewSave(Context context, @Nullable AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        this.context = context;

        init();
    }

    private void init()
    {
        setLayoutManager(new GridLayoutManager(context, 3));

        imageGalleryAdapter = new GridImageButtonAdapterSave(context);
        imageGalleryAdapter.setOnClickListener(new GridImageButtonAdapterSave.OnClickListener()
        {
            @Override
            public void onClick(View view, int position)
            {
                ImagePopupActivity popup = ImagePopupActivity.newInstance();
                popup.setImage(imageGalleryAdapter.getBitmap(position));
                popup.show(fragmentManager, "ImagePopupActivity");
            }

            @Override
            public void onLongClick(View view, int position)
            {
                clickedImage = position;

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Delete image?")
                        .setPositiveButton("Yes", removeImageDialogListener)
                        .setNegativeButton("No", removeImageDialogListener)
                        .show();
            }
        });
        this.setAdapter(imageGalleryAdapter);
    }

    public int addLoading()
    {
        int id = imageGalleryAdapter.addLoading();
        smoothScrollToPosition(imageGalleryAdapter.getItemCount() - 1);
        return id;
    }

    public void addBitmap(Bitmap thumbnail, Bitmap bitmap)
    {
        imageGalleryAdapter.addBitmap(thumbnail, bitmap);
        smoothScrollToPosition(imageGalleryAdapter.getItemCount() - 1);
    }

    public ArrayList<Bitmap> getBitmaps()
    {
        return imageGalleryAdapter.getBitmaps();
    }

    public ArrayList<Bitmap> getThumbnails()
    {
        return imageGalleryAdapter.getThumbnails();
    }

    public void doneLoading(int index, Bitmap thumbnail, Bitmap bitmap)
    {
        imageGalleryAdapter.setBitmaps(index, thumbnail, bitmap);
    }

    public void doneSaving(int index)
    {
        imageGalleryAdapter.setSaved(index);
    }

    public boolean doneLoading()
    {
        return imageGalleryAdapter.doneLoading();
    }
}
