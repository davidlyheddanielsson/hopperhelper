package davidlyheddanielsson.hopperhelper.views.imagegalleryview;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.support.v4.app.FragmentManager;

import com.stfalcon.frescoimageviewer.ImageViewer;

import java.net.URI;
import java.util.ArrayList;

import davidlyheddanielsson.hopperhelper.MoveManager;

public class ImageGalleryView extends RecyclerView
{
    private GridImageButtonAdapter imageGalleryAdapter;

    private Context context;

    private ArrayList<Uri> bitmapURIs = new ArrayList<>();

    public ImageGalleryView(Context context)
    {
        super(context);
        this.context = context;

        init();
    }

    public ImageGalleryView(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        this.context = context;

        init();
    }

    public ImageGalleryView(Context context, @Nullable AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        this.context = context;

        init();
    }

    private void init()
    {
        setLayoutManager(new GridLayoutManager(context, 3));

        imageGalleryAdapter = new GridImageButtonAdapter(context);
        imageGalleryAdapter.setOnClickListener(new GridImageButtonAdapter.OnClickListener()
        {
            @Override
            public void onClick(View view, int position)
            {
                new ImageViewer.Builder(context, bitmapURIs).setStartPosition(0).show();
                //ImagePopupActivity popup = ImagePopupActivity.newInstance();
                //popup.setImage(imageGalleryAdapter.getBitmap(position));
                //popup.show(fragmentManager, "ImagePopupActivity");
            }

            @Override
            public void onLongClick(View view, int position)
            { }
        });
        this.setAdapter(imageGalleryAdapter);
    }

    public void addBitmap(Uri thumbnailURI, Uri bitmapURI)
    {
        bitmapURIs.add(bitmapURI);

        imageGalleryAdapter.addBitmap(thumbnailURI, bitmapURI);
        smoothScrollToPosition(imageGalleryAdapter.getItemCount() - 1);
    }
}
