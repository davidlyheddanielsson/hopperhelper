package davidlyheddanielsson.hopperhelper.views.imagegalleryview;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.chrisbanes.photoview.PhotoView;

import davidlyheddanielsson.hopperhelper.R;

public class ImagePopupActivity extends DialogFragment
{
    private Bitmap image;
    public void setImage(Bitmap image)
    {
        this.image = image;
    }

    public static ImagePopupActivity newInstance()
    {
        return new ImagePopupActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.layout_imagegalleryview_popup, container, false);

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        PhotoView imageView = (PhotoView)view.findViewById(R.id.layout_imagegalleryview_image);

        imageView.setImageBitmap(image);
        imageView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });
    }
}
