package davidlyheddanielsson.hopperhelper.views.imagegalleryview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.facebook.drawee.view.SimpleDraweeView;

import java.lang.reflect.Array;
import java.net.URI;
import java.util.ArrayList;

import davidlyheddanielsson.hopperhelper.R;

public class GridImageButtonAdapter extends RecyclerView.Adapter<GridImageButtonAdapter.ViewHolder>
{
    public interface OnClickListener
    {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Uri> thumbnails = new ArrayList<>();
    private ArrayList<Uri> bitmaps = new ArrayList<>();
    private OnClickListener onClickListener;

    public GridImageButtonAdapter(Context context)
    {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void addBitmap(Uri thumbnail, Uri bitmap)
    {
        thumbnails.add(thumbnail);
        bitmaps.add(bitmap);

        notifyDataSetChanged();
    }

    public void setBitmaps(int index, Uri thumbnail, Uri bitmap)
    {
        thumbnails.set(index, thumbnail);
        bitmaps.set(index, bitmap);

        notifyDataSetChanged();
    }

    public Uri getThumbnail(int position)
    {
        return thumbnails.get(position);
    }

    public Uri getBitmap(int position)
    {
        return bitmaps.get(position);
    }

    public void removeBitmap(int index)
    {
        thumbnails.remove(index);
        bitmaps.remove(index);

        notifyDataSetChanged();
    }

    public void setOnClickListener(OnClickListener listener)
    {
        this.onClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = inflater.inflate(R.layout.layout_imagebuttongrid_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        Uri thumbnail = thumbnails.get(position);

        if(thumbnail != null)
        {
            //holder.button.setImageBitmap(thumbnails.get(position));
            holder.button.setImageURI(thumbnails.get(position));
            holder.button.setVisibility(View.VISIBLE);
            holder.button.setId(position);

            holder.progressBar.setVisibility(View.GONE);
        }
        else
        {
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.button.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount()
    {
        return thumbnails.size();
    }

    public ArrayList<Uri> getBitmaps()
    {
        return bitmaps;
    }

    public ArrayList<Uri> getThumbnails()
    {
        return thumbnails;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
    {
        SimpleDraweeView button;
        //ImageButton button;
        ImageView savedImage;
        ProgressBar progressBar;

        ViewHolder(View itemView)
        {
            super(itemView);

            this.button = (SimpleDraweeView)itemView.findViewById(R.id.layout_imagebuttongrid_button);
            this.savedImage = (ImageView)itemView.findViewById(R.id.layout_imagebuttongrid_savedImage);
            this.progressBar = (ProgressBar)itemView.findViewById(R.id.layout_imagebuttongrid_progressbar);

            this.button.setOnClickListener(this);
            this.button.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            if(onClickListener != null)
                onClickListener.onClick(v, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v)
        {
            if(onClickListener != null)
            {
                onClickListener.onLongClick(v, getAdapterPosition());
                return true;
            }

            return false;
        }
    }
}
