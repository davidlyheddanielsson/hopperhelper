package davidlyheddanielsson.hopperhelper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.SparseArray;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class FileUtils
{
    private static final String SCREENS_DIR_NAME = "screens";
    private static final String MOVES_DIR_NAME = "moves";
    private static final String THUMBNAIL_DIR_NAME = "thumbs";
    private static final String UNIQUEID_FILE_NAME = "uniqueid";
    private static final String DOWNLOADED_DIR_NAME = "downloaded";
    private static final String TEMP_DIR_NAME = "temp";

    private static File dirRoot;
    private static File dirSixes;
    private static File dirEights;
    private static File dirCharleston;
    private static File dirTemp;
    private static File dirDownloaded;

    private static File fileDownloadedInfo;

    private static Map<String, Integer> nameToSixID = new HashMap<>();
    private static Map<String, Integer> nameToEightID = new HashMap<>();
    private static Map<String, Integer> nameToCharlestonID = new HashMap<>();

    private static SparseArray<Move> idToMove = new SparseArray<>();

    private static ArrayList<DownloadedFile> downloadedFiles = new ArrayList<>();

    public static class ThumbedBitmap
    {
        public Uri thumbnailUri;
        public Uri bitmapUri;

        public ThumbedBitmap(Uri thumbnailUri, Uri bitmapUri)
        {
            this.thumbnailUri = thumbnailUri;
            this.bitmapUri = bitmapUri;
        }
    }

    public static class DownloadedFile
    {
        public File file;
        public String title;

        public DownloadedFile(File file, String title)
        {
            this.file = file;
            this.title = title;
        }
    }

    /**
     * Request the required storage permissions
     */
    public static void requestStoragePermissions(Activity activity, int requestCode)
    {
        String[] permissions = {
                Manifest.permission.READ_EXTERNAL_STORAGE
                , Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        if(ContextCompat.checkSelfPermission(activity, permissions[0]) != PERMISSION_GRANTED
           || ContextCompat.checkSelfPermission(activity, permissions[1]) != PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity,
                                              permissions,
                                              requestCode);
        }
        else
            activity.onRequestPermissionsResult(requestCode, permissions, new int[] { PERMISSION_GRANTED, PERMISSION_GRANTED });
    }

    /**
     * Creates the directory structure needed and initializes the uniqueID file
     *
     * @return False if any creation, deletion, read, or write fails
     */
    public static boolean init(Context context)
    {
        dirRoot = context.getExternalFilesDir(null);
        if(dirRoot == null)
        {
            Log.e("FileUtils", "externalDir is unavailable");
            return false;
        }

        final String EXTERNAL_DIR_MOVES = dirRoot + "/" + MOVES_DIR_NAME;
        dirSixes = new File(EXTERNAL_DIR_MOVES, "sixes");
        dirEights = new File(EXTERNAL_DIR_MOVES, "eights");
        dirCharleston = new File(EXTERNAL_DIR_MOVES, "charleston");
        dirTemp = new File(dirRoot, TEMP_DIR_NAME);
        dirDownloaded = new File(dirRoot, DOWNLOADED_DIR_NAME);

        // Create move dirs as well as thumbs dir
        if(!createMovesDir(dirSixes))
            return false;
        if(!createMovesDir(dirEights))
            return false;
        if(!createMovesDir(dirCharleston))
            return false;

        if(!createDirs(dirTemp))
            return false;
        if(!createDirs(dirDownloaded))
            return false;

        findDownloadedFiles();

        return true;
    }

    public static boolean deleteAllMoves()
    {
        cleanDirectory(dirRoot);

        if(!createMovesDir(dirSixes))
            return false;
        if(!createMovesDir(dirEights))
            return false;
        if(!createMovesDir(dirCharleston))
            return false;

        if(!createDirs(dirTemp))
            return false;
        if(!createDirs(dirDownloaded))
            return false;

        return true;
    }

    public static boolean deleteMove(int id)
    {
        Move move = idToMove.get(id);
        if(move == null)
            return false;
        else
            return deleteMove(move);
    }

    public static boolean deleteMove(Move move)
    {
        File directory;
        switch(move.getCategory())
        {
            case SIXES:
                directory = new File(dirSixes, MOVES_DIR_NAME);
                break;
            case EIGHTS:
                directory = new File(dirEights, MOVES_DIR_NAME);
                break;
            case CHARLESTON:
                directory = new File(dirCharleston, MOVES_DIR_NAME);
                break;
            default:
                return false;
        }

        File moveFile = new File(directory, String.valueOf(move.getUniqueID()));

        if(!moveFile.exists())
            return false;

        moveFile.delete();

        for(File screenshot : move.getScreenshots())
            screenshot.delete();
        for(File thumbnail : move.getThumbnails())
            thumbnail.delete();

        idToMove.delete(move.getUniqueID());

        switch(move.getCategory())
        {
            case SIXES:
                nameToSixID.remove(move.getName());
                break;
            case EIGHTS:
                nameToEightID.remove(move.getName());
                break;
            case CHARLESTON:
                nameToCharlestonID.remove(move.getName());
                break;
            default:
                throw new IllegalArgumentException("Unallowed move category");
        }

        return true;
    }

    private static void findDownloadedFiles()
    {
        fileDownloadedInfo = new File(dirDownloaded, "info.txt");

        try(BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileDownloadedInfo))))
        {
            String line;
            while((line = in.readLine()) != null)
            {
                String[] split = line.split(":");

                File file = new File(dirDownloaded, split[0]);
                downloadedFiles.add(new DownloadedFile(file, split[1]));
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public static ArrayList<DownloadedFile> getDownloadedFiles()
    {
        return downloadedFiles;
    }

    /**
     * Creates a folder (if it doesn't already exist) with a screens, and a thumbs subdirectory
     *
     * @param dir Directory to create and place filders inside
     *
     * @return False if any of the directories couldn't be created
     */
    private static boolean createMovesDir(File dir)
    {
        final File movesDir = new File(dir, MOVES_DIR_NAME);
        final File screensDir = new File(dir, SCREENS_DIR_NAME);
        final File thumbsDir = new File(dir, THUMBNAIL_DIR_NAME);

        if(!movesDir.exists() && !movesDir.mkdirs())
            return false;

        if(!screensDir.exists() && !screensDir.mkdirs())
            return false;

        if(!thumbsDir.exists() && !thumbsDir.mkdirs())
            return false;

        return true;
    }

    /**
     * Creates a folder if it doesn't already exist
     *
     * @param dir Directory to create
     *
     * @return False if the directory couldn't be created
     */
    private static boolean createDirs(File dir)
    {
        if(!dir.exists())
            return dir.mkdirs();

        return true;
    }

    /**
     * Gets a semi-unique ID and also writes the new id to file
     */
    public static int getUniqueID() throws IOException
    {
        File uniqueIDFile = new File(dirRoot, UNIQUEID_FILE_NAME);

        int returnID;

        if(uniqueIDFile.exists())
        {
            try(DataInputStream in = new DataInputStream(new FileInputStream(uniqueIDFile)))
            {
                returnID = in.readInt();
            }
        }
        else
            returnID = 0;

        try(DataOutputStream out = new DataOutputStream(new FileOutputStream(uniqueIDFile)))
        {
            out.writeInt(returnID + 1);
        }

        return returnID;
    }

    /**
     * Deletes the given directory and all subdirectories, even if they have files in them
     *
     * @return False if any deletion failed
     */
    private static boolean cleanDirectory(File dir)
    {
        if(dir.isDirectory())
        {
            for(File child : dir.listFiles())
            {
                if(!cleanDirectory(child))
                    return false;
            }
        }

        return dir.delete();
    }

    /**
     * Empties the temp directory
     *
     * @return False if deletion, or re-creation failed
     */
    public static boolean cleanTemp()
    {
        if(!cleanDirectory(dirTemp))
            return false;

        tempFiles.clear();

        return dirTemp.mkdirs();
    }

    /**
     * Reads all moves
     */
    public static void readMoves(ArrayList<Move> sixes, ArrayList<Move> eights, ArrayList<Move> charleston, SparseArray<Move> children)
    {
        try
        {
            File[] files = new File(dirSixes, MOVES_DIR_NAME).listFiles();
            if(files != null)
            {
                for(File moveFile : files)
                {

                    Move newMove = new Move(MoveCategory.SIXES,
                                            moveFile,
                                            new File(dirSixes, SCREENS_DIR_NAME),
                                            new File(dirSixes, THUMBNAIL_DIR_NAME));
                    if(newMove.getParentID() == -1)
                        sixes.add(newMove);
                    else
                        children.put(newMove.getUniqueID(), newMove);

                    nameToSixID.put(newMove.getName(), newMove.getUniqueID());
                    idToMove.put(newMove.getUniqueID(), newMove);
                }
            }

            files = new File(dirEights, MOVES_DIR_NAME).listFiles();
            if(files != null)
            {
                for(File moveFile : files)
                {

                    Move newMove = new Move(MoveCategory.EIGHTS,
                                            moveFile,
                                            new File(dirEights, SCREENS_DIR_NAME),
                                            new File(dirEights, THUMBNAIL_DIR_NAME));
                    if(newMove.getParentID() == -1)
                        eights.add(newMove);
                    else
                        children.put(newMove.getUniqueID(), newMove);

                    nameToEightID.put(newMove.getName(), newMove.getUniqueID());
                    idToMove.put(newMove.getUniqueID(), newMove);
                }
            }

            files = new File(dirCharleston, MOVES_DIR_NAME).listFiles();
            if(files != null)
            {
                for(File moveFile : files)
                {

                    Move newMove = new Move(MoveCategory.CHARLESTON,
                                            moveFile,
                                            new File(dirEights, SCREENS_DIR_NAME),
                                            new File(dirEights, THUMBNAIL_DIR_NAME));
                    if(newMove.getParentID() == -1)
                        charleston.add(newMove);
                    else
                        children.put(newMove.getUniqueID(), newMove);

                    nameToCharlestonID.put(newMove.getName(), newMove.getUniqueID());
                    idToMove.put(newMove.getUniqueID(), newMove);
                }
            }
        }
        catch(IOException e)
        {
            Log.e("FileUtils", "Error when trying to read move file", e);
        }
    }

    public static ArrayList<ThumbedBitmap> readMoveBitmaps(MoveCategory category, String moveName)
    {
        Move move;

        switch(category)
        {
            case SIXES:
            {
                move = idToMove.get(nameToSixID.get(moveName));
                break;
            }
            case EIGHTS:
            {
                move = idToMove.get(nameToEightID.get(moveName));
                break;
            }
            case CHARLESTON:
            {
                move = idToMove.get(nameToCharlestonID.get(moveName));
                break;
            }
            default:
                throw new IllegalArgumentException("Unallowed move category");
        }

        ArrayList<ThumbedBitmap> thumbedBitmaps = new ArrayList<>();

        for(int i = 0; i < move.getScreenshots().size(); ++i)
        {
                File thumbnailFile = move.getThumbnails().get(i);
                File screenFile = move.getScreenshots().get(i);

                thumbedBitmaps.add(new ThumbedBitmap(Uri.fromFile(thumbnailFile), Uri.fromFile(screenFile)));
        }

        return thumbedBitmaps;
    }

    public static File getDownloadFile(int size, String fileName, String videoTitle)
    {
        while(downloadedFiles.size() > 2)
        {
            if(!downloadedFiles.get(downloadedFiles.size() - 1).file.delete())
                break;
            downloadedFiles.remove(downloadedFiles.size() - 1);
        }

        File newFile = new File(dirDownloaded, fileName);
        downloadedFiles.add(0, new DownloadedFile(newFile, videoTitle));

        saveDownloadedInfo();

        return newFile;
    }

    private static void saveDownloadedInfo()
    {
        try(FileOutputStream out = new FileOutputStream(fileDownloadedInfo))
        {
            for(DownloadedFile downloadedFile : downloadedFiles)
                out.write((downloadedFile.file.getName() + ":" + downloadedFile.title + '\n').getBytes());
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public static File getDownloadedFileByName(String name)
    {
        for(int i = 0; i < downloadedFiles.size(); i++)
        {
            DownloadedFile downloadedFile = downloadedFiles.get(i);

            if(downloadedFile.file.getName().equals(name))
            {
                downloadedFiles.remove(i);
                downloadedFiles.add(0, downloadedFile);
                return downloadedFile.file;
            }
        }

        return null;
    }

    public static File getDownloadedFileByTitle(String title)
    {
        for(int i = 0; i < downloadedFiles.size(); i++)
        {
            DownloadedFile downloadedFile = downloadedFiles.get(i);

            if(downloadedFile.title.equals(title))
            {
                downloadedFiles.remove(i);
                downloadedFiles.add(0, downloadedFile);
                return downloadedFile.file;
            }
        }

        return null;
    }

    private static ArrayList<File> tempFiles = new ArrayList<>();

    public static ArrayList<File> getTempFiles()
    {
        return tempFiles;
    }

    public static boolean saveTemp(Bitmap bitmap, String name)
    {
        File tempFile = saveTempWithoutAddingToList(bitmap, name);

        if(tempFile == null)
            return false;

        addToTempFiles(tempFile);

        return true;
    }

    public static File saveTempWithoutAddingToList(Bitmap bitmap, String name)
    {
        File outFile = new File(dirTemp, name);

        try(FileOutputStream out = new FileOutputStream(outFile))
        {
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, out);
        }
        catch(IOException e)
        {
            Log.e("FileUilts", String.format("Error when trying to save file to %s", outFile.toString()), e);
            return null;
        }

        return outFile;
    }

    public static void addToTempFiles(File file)
    {
        tempFiles.add(file);
    }

    public static void deleteFromTempFiles(File file)
    {
        file.delete();
        tempFiles.remove(file);
    }

    public static Move createMoveFromTemp(MoveCategory category, String moveName) // TODO: Cleanup (or something) if this returns false
    {
        int uniqueID;
        try
        {
            uniqueID = getUniqueID();
        }
        catch(IOException e)
        {
            e.printStackTrace();
            return null;
        }

        ArrayList<File> screenshots = new ArrayList<>(tempFiles.size() / 2);
        ArrayList<File> thumbnails = new ArrayList<>(tempFiles.size() / 2);

        // TODO: Sorting?

        for(int i = 0; i < tempFiles.size(); i++)
        {
            File file = tempFiles.get(i);

            if(file.getName().contains("_t"))
                thumbnails.add(file);
            else
                screenshots.add(file);
        }

        if(thumbnails.size() != screenshots.size())
        {
            Log.e("FileUtils", String.format("Mismatched screenshots/thumbnails, there are %d screenshots and %d thumbnails", thumbnails.size(), screenshots.size()));
            return null;
        }

        tempFiles.clear();

        return new Move(uniqueID, -1, new ArrayList<Integer>(), moveName, category, screenshots, thumbnails);
    }

    public static boolean storeMove(Move move)
    {
        // TODO: Make sure move doesn't exist

        File rootDirectory;

        Map<String, Integer> nameToID;

        switch(move.getCategory())
        {
            case SIXES:
                rootDirectory = dirSixes;

                nameToID = nameToSixID;
                break;
            case EIGHTS:
                rootDirectory = dirEights;

                nameToID = nameToEightID;
                break;
            case CHARLESTON:
                rootDirectory = dirCharleston;

                nameToID = nameToCharlestonID;
                break;
            default:
                throw new IllegalArgumentException("Unallowed move category");
        }

        File moveDirectory = new File(rootDirectory, MOVES_DIR_NAME);

        File screenshotDir = new File(rootDirectory, SCREENS_DIR_NAME);
        File thumbsDir = new File(rootDirectory, THUMBNAIL_DIR_NAME);

        ArrayList<File> screenshots = move.getScreenshots();
        ArrayList<File> thumbnails = move.getThumbnails();

        for(int i = 0; i < screenshots.size(); i++)
        {
            File file = screenshots.get(i);

            File outFile = new File(screenshotDir, String.valueOf(move.getUniqueID()) + "_" + i + ".png");
            if(!file.renameTo(outFile))
            {
                Log.e("FileUtils", "Couldn't move screenshot from \"" + file.getAbsolutePath() + "\" to \"" + outFile.getAbsolutePath() + "\"");
                return false;
            }

            screenshots.set(i, outFile);
        }

        for(int i = 0; i < thumbnails.size(); i++)
        {
            File file = thumbnails.get(i);

            File outFile = new File(thumbsDir, String.valueOf(move.getUniqueID()) + "_" + i + ".png");
            if(!file.renameTo(outFile))
            {
                Log.e("FileUtils", "Couldn't move screenshot from \"" + file.getAbsolutePath() + "\" to \"" + outFile.getAbsolutePath() + "\"");
                return false;
            }

            thumbnails.set(i, outFile);
        }

        try
        {
            File moveFile = new File(moveDirectory, String.valueOf(move.getUniqueID()));
            move.writeTo(moveFile);

            nameToID.put(move.getName(), move.getUniqueID());
            idToMove.put(move.getUniqueID(), move);

            return true;
        }
        catch(IOException e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean replaceMoveFile(Move move)
    {
        File rootDirectory;

        switch(move.getCategory())
        {
            case SIXES:
                rootDirectory = dirSixes;
                break;
            case EIGHTS:
                rootDirectory = dirEights;
                break;
            case CHARLESTON:
                rootDirectory = dirCharleston;
                break;
            default:
                throw new IllegalArgumentException("Unallowed move category");
        }

        File moveDirectory = new File(rootDirectory, MOVES_DIR_NAME);
        File moveFile = new File(moveDirectory, String.valueOf(move.getUniqueID()));
        try
        {
            if(!moveFile.exists())
                return false;

            move.writeTo(moveFile);
            return true;
        }
        catch(IOException e)
        {
            e.printStackTrace();
            return false;
        }
    }
}
