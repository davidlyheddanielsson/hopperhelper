package davidlyheddanielsson.hopperhelper;

public enum MoveCategory
{
    SIXES, EIGHTS, CHARLESTON;

    public static MoveCategory fromInt(int value)
    {
        switch(value)
        {
            case 0:
                return SIXES;
            case 1:
                return EIGHTS;
            case 2:
                return CHARLESTON;
            default:
                throw new IllegalArgumentException("Cannot create MoveCateogry from value" + value);
        }
    }

    public static int toInt(MoveCategory category)
    {
        switch(category)
        {
            case SIXES:
                return 0;
            case EIGHTS:
                return 1;
            case CHARLESTON:
                return 2;
            default:
                throw new IllegalArgumentException("Cannot create int from this category");
        }
    }
}
