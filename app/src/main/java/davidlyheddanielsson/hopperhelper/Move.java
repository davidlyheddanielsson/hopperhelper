package davidlyheddanielsson.hopperhelper;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Move implements Parcelable
{
    private int uniqueID;
    private int parentID;
    private ArrayList<Integer> children;
    private String name;
    private MoveCategory category;

    private ArrayList<File> screenshots;
    private ArrayList<File> thumbnails;

    public Move(int uniqueID,
                int parentID,
                ArrayList<Integer> children,
                String name,
                MoveCategory category,
                ArrayList<File> screenshots,
                ArrayList<File> thumbnails)
    {
        this.uniqueID = uniqueID;
        this.parentID = parentID;
        this.children = children;
        this.name = name;
        this.category = category;
        this.screenshots = screenshots;
        this.thumbnails = thumbnails;
    }

    public Move(MoveCategory category, File inFile, File screensDir, File thumbnailsDir) throws IOException
    {
        try(DataInputStream in = new DataInputStream(new FileInputStream(inFile)))
        {
            this.uniqueID = Integer.parseInt(inFile.getName());
            this.parentID = in.readInt();

            int childCount = in.readInt();
            this.children = new ArrayList<>();
            for(int i = 0; i < childCount; ++i)
                children.add(in.readInt());

            byte[] nameBytes = new byte[in.readInt()];
            in.read(nameBytes);
            this.name = new String(nameBytes);

            this.category = category;

            int screenshotCount = in.readInt();
            this.screenshots = new ArrayList<>(screenshotCount);
            this.thumbnails = new ArrayList<>(screenshotCount);

            String uniqueIDString = String.valueOf(this.uniqueID);

            for(int i = 0; i < screenshotCount; ++i)
            {
                File screenshotFile = new File(screensDir, uniqueIDString + "_" + i + ".png");
                if(!screenshotFile.exists())
                    throw new FileNotFoundException("Couldn't find screenshot file at " + screenshotFile.getAbsolutePath());

                screenshots.add(screenshotFile);
            }
            for(int i = 0; i < screenshotCount; ++i)
            {
                File thumbnailFile = new File(thumbnailsDir, uniqueIDString + "_" + i + ".png");
                if(!thumbnailFile.exists())
                    throw new FileNotFoundException("Couldn't find thumbnail file at " + thumbnailFile.getAbsolutePath());

                thumbnails.add(thumbnailFile);
            }
        }
    }

    public void writeTo(File file) throws IOException
    {
        try(DataOutputStream out = new DataOutputStream(new FileOutputStream(file)))
        {
            out.writeInt(parentID);
            out.writeInt(children.size());
            for(Integer childID : children)
                out.writeInt(childID);

            out.writeInt(name.length());
            out.writeBytes(name);
            out.writeInt(screenshots.size());
        }
    }

    public int getUniqueID()
    {
        return uniqueID;
    }

    public ArrayList<Integer> getChildren()
    {
        return children;
    }

    public String getName()
    {
        return name;
    }

    public MoveCategory getCategory()
    {
        return category;
    }

    public ArrayList<File> getScreenshots()
    {
        return screenshots;
    }

    public ArrayList<File> getThumbnails()
    {
        return thumbnails;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(this.uniqueID);
        dest.writeInt(this.parentID);
        dest.writeInt(this.children.size());
        for(Integer childID : children)
            dest.writeInt(childID);
        dest.writeString(this.name);
        dest.writeSerializable(category);
        dest.writeInt(screenshots.size());
        for(File file : screenshots)
            dest.writeString(file.getAbsolutePath());
        for(File file : thumbnails)
            dest.writeString(file.getAbsolutePath());
    }

    public static final Parcelable.Creator<Move> CREATOR = new Parcelable.Creator<Move>()
    {
        @Override
        public Move createFromParcel(Parcel source)
        {
            int uniqueID = source.readInt();
            int parentID = source.readInt();

            int childCount = source.readInt();
            ArrayList<Integer> children = new ArrayList<>();
            for(int i = 0; i < childCount; ++i)
                children.add(source.readInt());

            String name = source.readString();
            MoveCategory category = (MoveCategory)source.readSerializable();

            int screenshotCount = source.readInt();
            ArrayList<File> screenshots = new ArrayList<>();
            ArrayList<File> thumbnails = new ArrayList<>();

            for(int i = 0; i < screenshotCount; ++i)
                screenshots.add(new File(source.readString()));
            for(int i = 0; i < screenshotCount; ++i)
                thumbnails.add(new File(source.readString()));

            return new Move(uniqueID, parentID, children, name, category, screenshots, thumbnails);
        }

        @Override
        public Move[] newArray(int size)
        {
            return new Move[size];
        }
    };

    public int getParentID()
    {
        return parentID;
    }

    public void setParentID(int parentID)
    {
        this.parentID = parentID;
    }
}
