package davidlyheddanielsson.hopperhelper;

import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;

import davidlyheddanielsson.hopperhelper.activity.ShowMoveFragment;
import davidlyheddanielsson.hopperhelper.views.moveview.MoveView;

public class MoveManager
{
    private static ArrayList<Move> sixes = new ArrayList<>();
    private static ArrayList<Move> eights = new ArrayList<>();
    private static ArrayList<Move> charleston = new ArrayList<>();
    private static SparseArray<Move> childMoves = new SparseArray<>();

    private ToggleButton sixesButton;
    private ToggleButton eightsButton;
    private ToggleButton charlestonButton;

    private MoveView moveView;

    private static boolean readMoves = false;

    private AppCompatActivity activity;

    public void init(final AppCompatActivity activity,
                     MoveView moveView,
                     ToggleButton sixesButton,
                     ToggleButton eightsButton,
                     ToggleButton charlestonButton)
    {
        if(!readMoves)
        {
            FileUtils.readMoves(sixes, eights, charleston, childMoves);
            readMoves = true;
        }

        this.moveView = moveView;
        this.sixesButton = sixesButton;
        this.eightsButton = eightsButton;
        this.charlestonButton = charlestonButton;
        this.activity = activity;

        setListeners(activity);
        setColors(activity);
        updateShownMoves();
    }

    private void setListeners(final AppCompatActivity activity)
    {
        sixesButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(sixesButton.isChecked())
                    sixesButton.setBackgroundColor(activity.getColor(R.color.colorSix));
                else
                    sixesButton.setBackgroundColor(activity.getColor(R.color.colorAccent));

                updateShownMoves();
            }
        });

        eightsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(eightsButton.isChecked())
                    eightsButton.setBackgroundColor(activity.getColor(R.color.colorEight));
                else
                    eightsButton.setBackgroundColor(activity.getColor(R.color.colorAccent));

                updateShownMoves();
            }
        });

        charlestonButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(charlestonButton.isChecked())
                    charlestonButton.setBackgroundColor(activity.getColor(R.color.colorCharleston));
                else
                    charlestonButton.setBackgroundColor(activity.getColor(R.color.colorAccent));

                updateShownMoves();
            }
        });

        moveView.setOnMoveClickListener(new MoveView.OnMoveClickListener()
        {
            @Override
            public void OnMoveClick(View view, final Move move)
            {
                // TODO: Categories
                final ShowMoveFragment fragment = ShowMoveFragment.newInstance(move.getCategory(),
                                                                               move.getName());


                final DialogInterface.OnClickListener removeImageDialogListener = new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        switch(which)
                        {
                            case DialogInterface.BUTTON_POSITIVE:
                                deleteMove(move);
                                updateShownMoves();
                                fragment.dismiss();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };

                fragment.setDeleteOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        new AlertDialog.Builder(
                                activity).setMessage(
                                "Delete move?")
                                .setPositiveButton(
                                        "Yes",
                                        removeImageDialogListener)
                                .setNegativeButton(
                                        "No",
                                        removeImageDialogListener)
                                .show();
                    }
                });

                fragment.show(activity.getSupportFragmentManager(), "dialog");
            }

            @Override
            public void OnMoveLongClick(View view, Move move)
            {
                Toast.makeText(activity, "Held " + move.getName(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static ArrayList<Move> getSixes()
    {
        return sixes;
    }

    public static ArrayList<Move> getEights()
    {
        return eights;
    }

    public static ArrayList<Move> getCharleston()
    {
        return charleston;
    }

    public void deleteMove(Move move)
    {
        ArrayList<Move> targetList = null;

        switch(move.getCategory())
        {
            case SIXES:
                targetList = sixes;
                break;
            case EIGHTS:
                targetList = eights;
                break;
            case CHARLESTON:
                targetList = charleston;
                break;
        }

        for(int i = 0; i < targetList.size(); ++i)
        {
            Move candidate = targetList.get(i);

            if(candidate == move)
            {
                for(Integer id : move.getChildren())
                    FileUtils.deleteMove(id);

                targetList.remove(i);
                FileUtils.deleteMove(move);
                return;
            }
            else
            {
                if(candidate.getChildren().contains(move.getUniqueID()))
                {
                    FileUtils.deleteMove(move);

                    for(int j = 0; j < candidate.getChildren().size(); ++j)
                    {
                        if(candidate.getChildren().get(j) == move.getUniqueID())
                        {
                            candidate.getChildren().remove(j);
                            return;
                        }
                    }
                }
            }
        }
    }

    public void addMove(String parentName, Move move)
    {
        ArrayList<Move> targetList = null;

        switch(move.getCategory())
        {
            case SIXES:
                targetList = sixes;
                break;
            case EIGHTS:
                targetList = eights;
                break;
            case CHARLESTON:
                targetList = charleston;
                break;
        }

        if(!parentName.isEmpty())
        {
            boolean foundParent = false;
            for(Move toplistMove : targetList)
            {
                if(toplistMove.getName().equals(parentName))
                {
                    foundParent = true;

                    toplistMove.getChildren().add(move.getUniqueID());
                    childMoves.put(move.getUniqueID(), move);
                    move.setParentID(toplistMove.getUniqueID());

                    FileUtils.replaceMoveFile(toplistMove);

                    break;
                }
            }

            if(!foundParent)
            {
                Log.e("MoveManager", "Couldn't find move parent with id " + move.getParentID());
                targetList.add(move); // Safeguard
            }
        }
        else
            targetList.add(move);

        FileUtils.storeMove(move);

        updateShownMoves();
    }

    private void updateShownMoves()
    {
        moveView.setChildren(childMoves);

        if(sixesButton.isChecked())
            moveView.setSixes(sixes);
        else
            moveView.setSixes(new ArrayList<Move>());

        if(eightsButton.isChecked())
            moveView.setEights(eights);
        else
            moveView.setEights(new ArrayList<Move>());

        if(charlestonButton.isChecked())
            moveView.setCharleston(charleston);
        else
            moveView.setCharleston(new ArrayList<Move>());
    }

    private void setColors(AppCompatActivity activity)
    {
        if(sixesButton.isChecked())
            sixesButton.setBackgroundColor(activity.getColor(R.color.colorSix));
        else
            sixesButton.setBackgroundColor(activity.getColor(R.color.colorAccent));

        if(eightsButton.isChecked())
            eightsButton.setBackgroundColor(activity.getColor(R.color.colorEight));
        else
            eightsButton.setBackgroundColor(activity.getColor(R.color.colorAccent));

        if(charlestonButton.isChecked())
            charlestonButton.setBackgroundColor(activity.getColor(R.color.colorCharleston));
        else
            charlestonButton.setBackgroundColor(activity.getColor(R.color.colorAccent));
    }
}
