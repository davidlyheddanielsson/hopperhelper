package davidlyheddanielsson.hopperhelper.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import davidlyheddanielsson.hopperhelper.FileUtils;
import davidlyheddanielsson.hopperhelper.FileUtils.ThumbedBitmap;
import davidlyheddanielsson.hopperhelper.R;
import davidlyheddanielsson.hopperhelper.MoveCategory;
import davidlyheddanielsson.hopperhelper.views.imagegalleryview.ImageGalleryView;

public class ShowMoveFragment extends DialogFragment
{
    private static String ARGUMENT_TEXT = "text";
    private static String ARGUMENT_CATEGORY = "category";

    private View.OnClickListener deleteButtonListener;
    private View.OnClickListener editButtonListener;

    public static ShowMoveFragment newInstance(MoveCategory category,
                                               String text)
    {
        ShowMoveFragment fragment = new ShowMoveFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARGUMENT_CATEGORY, category);
        args.putString(ARGUMENT_TEXT, text);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.activity_show_move_fragment, container, false);

        MoveCategory moveCategory = (MoveCategory)getArguments().getSerializable(ARGUMENT_CATEGORY);
        String moveName = getArguments().getString(ARGUMENT_TEXT);

        ArrayList<ThumbedBitmap> thumbnails = FileUtils.readMoveBitmaps(moveCategory, moveName);

        ImageGalleryView gallery = (ImageGalleryView)view.findViewById(R.id.showmove_fragment_gallery);
        for(ThumbedBitmap pair : thumbnails)
            gallery.addBitmap(pair.thumbnailUri, pair.bitmapUri);

        TextView textView = (TextView)view.findViewById(R.id.showmove_fragment_title);
        textView.setText(moveName);

        view.findViewById(R.id.showmove_fragment_delete).setOnClickListener(deleteButtonListener);
        //editButton = (ImageView)view.findViewById(R.id.showmove_fragment_edit);

        return view;
    }

    public void setDeleteOnClickListener(View.OnClickListener listener)
    {
        deleteButtonListener = listener;
    }

    public void setEditButtonListener(View.OnClickListener listener)
    {
        editButtonListener = listener;
    }
}