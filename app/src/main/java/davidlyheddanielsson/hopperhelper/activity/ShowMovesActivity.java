package davidlyheddanielsson.hopperhelper.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ToggleButton;

import com.facebook.drawee.backends.pipeline.Fresco;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import davidlyheddanielsson.hopperhelper.FileUtils;
import davidlyheddanielsson.hopperhelper.Move;
import davidlyheddanielsson.hopperhelper.MoveCategory;
import davidlyheddanielsson.hopperhelper.MoveManager;
import davidlyheddanielsson.hopperhelper.R;
import davidlyheddanielsson.hopperhelper.views.moveview.MoveView;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class ShowMovesActivity extends AppCompatActivity
{
    private static final int RESULT_ADD_MOVE = 0;

    private static final int PERMISSION_STORAGE = 1;

    MoveManager moveManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_moves);
        Fresco.initialize(this);

        moveManager = new MoveManager();

        String downloadURL = null;

        Intent startIntent = getIntent();
        if(startIntent.getAction().equals("android.intent.action.SEND"))
        {
            downloadURL = getIntent().getClipData().getItemAt(0).getText().toString();
        }

        FileUtils.requestStoragePermissions(this, PERMISSION_STORAGE);

        setListeners();

        if(downloadURL != null)
        {
            Intent intent = new Intent(ShowMovesActivity.this, AddMoveActivity.class);
            AddMoveActivity.addExtras(intent, MoveCategory.SIXES, downloadURL); // TODO
            startActivityForResult(intent, RESULT_ADD_MOVE);
        }
    }

    private void init()
    {
        if(!FileUtils.init(this))
            throw new RuntimeException("Couldn't init files");
        //FileUtils.deleteAllMoves();

        MoveView moveView = (MoveView)findViewById(R.id.showmoves_moveview);
        ToggleButton sixesButton = (ToggleButton)findViewById(R.id.showmoves_button_sixes);
        ToggleButton eightsButton = (ToggleButton)findViewById(R.id.showmoves_button_eights);
        ToggleButton charlestonButton = (ToggleButton)findViewById(R.id.showmoves_button_charleston);

        moveManager.init(this, moveView, sixesButton, eightsButton, charlestonButton);

        /*final int MOVES = 50;

        int moveUniqueID = 0;
        int childUniqueID = MOVES;

        Random random = new Random();

        for(int i = 0; i < MOVES; ++i)
        {
            Move newMove = new Move(moveUniqueID, -1, new ArrayList<Integer>(), "Move " + i, MoveCategory.fromInt(i % 3), new ArrayList<File>(), new ArrayList<File>());
            moveManager.addMove("", newMove);

            int childCount = random.nextInt(7);
            for(int child = 0; child < childCount; ++child)
            {
                moveManager.addMove("Move " + i, new Move(childUniqueID, moveUniqueID, new ArrayList<Integer>(), "Child " + child + "/" + childCount, MoveCategory.fromInt(i % 3), new ArrayList<File>(), new ArrayList<File>()));

                childUniqueID++;
            }

            moveUniqueID++;
        }*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == RESULT_ADD_MOVE)
        {
            if(resultCode == RESULT_OK)
            {
                moveManager.addMove(AddMoveActivity.getParentName(data), AddMoveActivity.getMove(data));
            }
        }
        else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        if(requestCode == PERMISSION_STORAGE)
        {
            boolean hasAllPermissions = true;

            for(int result : grantResults)
            {
                if(result != PERMISSION_GRANTED)
                {
                    hasAllPermissions = false;
                    break;
                }
            }

            if(hasAllPermissions)
                init();
            else
            {
                DialogInterface.OnCancelListener dialogCancelListener = new DialogInterface.OnCancelListener()
                {
                    @Override
                    public void onCancel(DialogInterface dialog)
                    {
                        finishAffinity();
                    }
                };

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        switch(which)
                        {
                            case DialogInterface.BUTTON_POSITIVE:
                                FileUtils.requestStoragePermissions(ShowMovesActivity.this, PERMISSION_STORAGE);
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                finishAffinity();
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(
                        "The app requires the requested permissions to function. Ask again? If no, the app will close")
                        .setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener)
                        .setOnCancelListener(dialogCancelListener)
                        .show().setCanceledOnTouchOutside(true);

            }
        }
        else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void setListeners()
    {
        final FloatingActionButton addMoveButton = (FloatingActionButton)findViewById(R.id.showmoves_fab_addmove);
        addMoveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(ShowMovesActivity.this, AddMoveActivity.class);
                AddMoveActivity.addExtras(intent, MoveCategory.SIXES); // TODO
                startActivityForResult(intent, RESULT_ADD_MOVE);
            }
        });

        final MoveView moveView = (MoveView)findViewById(R.id.showmoves_moveview);
        moveView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(addMoveButton.isShown() && dy > 0)
                    addMoveButton.hide();
                else if(!addMoveButton.isShown() && dy < 0)
                    addMoveButton.show();
            }
        });
    }
}
