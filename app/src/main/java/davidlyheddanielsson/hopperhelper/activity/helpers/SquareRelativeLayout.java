package davidlyheddanielsson.hopperhelper.activity.helpers;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

public class SquareRelativeLayout extends RelativeLayout
{
    public SquareRelativeLayout(@NonNull Context context)
    {
        super(context);
    }

    public SquareRelativeLayout(@NonNull Context context,
                                @Nullable AttributeSet attrs)
    {
        super(context, attrs);
    }

    public SquareRelativeLayout(@NonNull Context context,
                                @Nullable AttributeSet attrs,
                                @AttrRes int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    public SquareRelativeLayout(@NonNull Context context,
                                @Nullable AttributeSet attrs,
                                @AttrRes int defStyleAttr,
                                @StyleRes int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
