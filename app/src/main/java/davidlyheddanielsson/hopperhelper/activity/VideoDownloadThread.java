package davidlyheddanielsson.hopperhelper.activity;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.commit451.youtubeextractor.YouTubeExtractionResult;
import com.commit451.youtubeextractor.YouTubeExtractor;
import com.dinuscxj.progressbar.CircleProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;

import davidlyheddanielsson.hopperhelper.FileUtils;

public class VideoDownloadThread extends AsyncTask<Void, Void, Boolean>
{
    public interface OnDownloadCompleteListener
    {
        void onDownloadComplete(Boolean success, File outFile);
    }

    private int downloadedSize;
    private int totalSize;

    private File outFile;
    private String youtubeID;

    private TextView progressText;
    private CircleProgressBar progressBar;

    private OnDownloadCompleteListener onDownloadComplete = null;

    public VideoDownloadThread(TextView progressText,
                               CircleProgressBar progressBar,
                               String youtubeID)
    {
        this.progressText = progressText;
        this.progressBar = progressBar;
        this.youtubeID = youtubeID;

        this.outFile = null;

        downloadedSize = 0;
        totalSize = -1;
    }

    @Override
    protected void onPreExecute()
    {
        progressText.setText("Starting download...");

        progressBar.setProgress(0);
        progressBar.setMax(100);
    }

    @Override
    protected Boolean doInBackground(Void... textViews)
    {
        String youtubeTitle = getYoutubeTitle(youtubeID);

        YouTubeExtractor extractor = YouTubeExtractor.create();
        YouTubeExtractionResult result = extractor.extract(youtubeID)
                .blockingGet();

        Uri downloadURI = result.getHd720VideoUri();

        if(downloadURI == null)
            downloadURI = result.getBestAvailableQualityVideoUri();
        if(downloadURI == null)
            return false;

        InputStream inStream = null;

        try
        {
            URL downloadURL = new URL(Uri.decode(downloadURI.toString()));
            HttpURLConnection conn = (HttpURLConnection)downloadURL.openConnection();

            totalSize = conn.getContentLength();

            outFile = FileUtils.getDownloadFile(totalSize, youtubeID, youtubeTitle);

            inStream = downloadURL.openStream();

            FileOutputStream out = new FileOutputStream(outFile);
            byte[] buffer = new byte[1024];
            int len = 0;

            long zero = System.nanoTime();
            while((len = inStream.read(buffer)) > 0)
            {
                out.write(buffer, 0, len);
                downloadedSize += len;

                long time = System.nanoTime();
                if(time - zero > 1e8) // 100 ms
                {
                    this.publishProgress();
                    zero = time;
                }
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
            return false;
            // TODO: Error handling, all over
        }
        finally
        {
            if(inStream != null)
            {
                try
                {
                    inStream.close();
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }
            }
        }

        return true;
    }

    private String getYoutubeTitle(String youtubeID)
    {
        try(InputStream inputStream = new URL("https://www.googleapis.com/youtube/v3/videos?part=snippet&id=" + youtubeID + "&key=AIzaSyAlw0xgbmK5hQ68zOAqQ2yaud-eK9FZPG8").openStream();
            ByteArrayOutputStream result = new ByteArrayOutputStream())
        {
            byte[] buffer = new byte[1024];
            int length;

            while((length = inputStream.read(buffer)) != -1)
                result.write(buffer, 0, length);

            JSONObject json = new JSONObject(result.toString());

            return json.getJSONArray("items").getJSONObject(0).getJSONObject("snippet").get("title").toString();
        }
        catch(IOException e)
        {
            Log.e("VideoDownloadThread", "Couldn't get info from youtube data API: " + e.toString());
        }
        catch(JSONException e)
        {
            Log.e("VideoDownloadThread", "Couldn't parse JSON from downloaded youtube data: " + e.toString());
        }

        return null;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean)
    {
        progressText.setText("Click here to screenshot");
        progressBar.setProgress(100);

        if(onDownloadComplete != null)
            onDownloadComplete.onDownloadComplete(aBoolean, outFile);
    }

    @Override
    protected void onProgressUpdate(Void... values)
    {
        super.onProgressUpdate(values);

        float progress = downloadedSize/(float)totalSize;

        progressBar.setProgress(Math.round(progress * 100));

        DecimalFormat format = new DecimalFormat("#.##MB");
        format.setMinimumFractionDigits(3);
        String totalMB = format.format(totalSize / 1000000.0f);
        String downloadedMB = format.format(downloadedSize / 1000000.0f);

        progressText.setText(downloadedMB + " / " + totalMB);
    }

    public void setOnDownloadCompleteListener(OnDownloadCompleteListener onDownloadCompleteListener)
    {
        this.onDownloadComplete = onDownloadCompleteListener;
    }

    public int getTotalSize()
    {
        return totalSize;
    }

    public int getDownloadedSize()
    {
        return downloadedSize;
    }
}
