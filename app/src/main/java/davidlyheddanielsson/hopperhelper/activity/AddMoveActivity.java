package davidlyheddanielsson.hopperhelper.activity;

import android.app.Activity;
import android.content.ClipboardManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dinuscxj.progressbar.CircleProgressBar;

import java.io.File;
import java.util.ArrayList;

import davidlyheddanielsson.hopperhelper.FileUtils;
import davidlyheddanielsson.hopperhelper.Move;
import davidlyheddanielsson.hopperhelper.MoveCategory;
import davidlyheddanielsson.hopperhelper.MoveManager;
import davidlyheddanielsson.hopperhelper.R;
import davidlyheddanielsson.hopperhelper.views.imagegalleryview.ImageGalleryView;
import davidlyheddanielsson.hopperhelper.views.stringlist.StringList;

public class AddMoveActivity extends AppCompatActivity
{
    private static final int REQUEST_PICK_VIDEO = 0;
    private static final int REQUEST_SCREENSHOT = 3;

    private static final String RESULT_MOVE = "movename";
    private static final String RESULT_PARENT_NAME = "parentname";

    private static final String EXTRA_TYPE = "type";
    private static final String EXTRA_DOWNLOAD_URL = "url";

    public static void addExtras(Intent intent, MoveCategory activityType)
    {
        intent.putExtra(EXTRA_TYPE, activityType);
    }

    public static void addExtras(Intent intent, MoveCategory activityType, String downloadURL)
    {
        intent.putExtra(EXTRA_TYPE, activityType);
        intent.putExtra(EXTRA_DOWNLOAD_URL, downloadURL);
    }

    public static String getParentName(Intent intent)
    {
        return intent.getExtras().getString(RESULT_PARENT_NAME);
    }

    public static Move getMove(Intent intent)
    {
        return (Move)intent.getExtras().get(RESULT_MOVE);
    }

    private ActionBar actionBar;

    private MoveCategory moveCategory = MoveCategory.SIXES;

    private LinearLayout rootLayout;

    private ImageButton downloadFromClipboardButton;
    private ImageButton downloadButton;
    private ImageButton galleryButton;

    private EditText nameEditText;
    private AutoCompleteTextView parentEditText;

    private Spinner spinner;
    private ImageButton saveButton;

    private void initUI()
    {
        setContentView(R.layout.activity_add_move);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        assert actionBar != null;

        actionBar.setDisplayShowTitleEnabled(false);
    }

    private void findViews()
    {
        rootLayout = (LinearLayout)findViewById(R.id.addmove_content_root);
        spinner = (Spinner)findViewById(R.id.addmove_spinner_type);
        nameEditText = (EditText)findViewById(R.id.addmove_text_movename);
        parentEditText = (AutoCompleteTextView)findViewById(R.id.addmove_text_parentname);
        downloadFromClipboardButton = (ImageButton)findViewById(R.id.addmove_button_clipboardpaste);
        downloadButton = (ImageButton)findViewById(R.id.addmove_button_download);
        galleryButton = (ImageButton)findViewById(R.id.addmove_button_gallery);
        saveButton = (ImageButton)findViewById(R.id.addmove_button_save);
    }

    private void initViews(MoveCategory category)
    {
        String[] spinnerItems = getResources().getStringArray(R.array.addmove_spinner_type);
        spinner.setAdapter(new ArrayAdapter<>(this,
                                              R.layout.titlebar_spinner_item,
                                              spinnerItems));
        spinner.setSelection(MoveCategory.toInt(category));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                moveCategory = MoveCategory.fromInt(position);
                setColors(moveCategory);
                updateParentSuggestions();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            { }
        });

        downloadFromClipboardButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ClipboardManager clipboardManager = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
                if(!clipboardManager.hasPrimaryClip())
                {
                    Toast.makeText(AddMoveActivity.this, "No clipboard data", Toast.LENGTH_SHORT).show();
                    return;
                }

                downloadFileAndStartActivity(clipboardManager.getPrimaryClip().getItemAt(0).getText().toString());
            }
        });

        downloadButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(AddMoveActivity.this);

                alert.setTitle("Youtube video");
                alert.setMessage("Enter youtube URL");

                final EditText input = new EditText(AddMoveActivity.this);
                alert.setView(input);

                alert.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        downloadFileAndStartActivity(input.getText().toString());
                    }
                });

                alert.setNegativeButton("Cancel", null);
                alert.show();
            }
        });

        galleryButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("video/*");
                startActivityForResult(Intent.createChooser(intent, "Select video"),
                                       REQUEST_PICK_VIDEO);
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String moveName = nameEditText.getText().toString().trim();
                String parentName = parentEditText.getText().toString().trim();

                Move newMove = FileUtils.createMoveFromTemp(moveCategory, moveName);

                if(newMove == null)
                {
                    Toast.makeText(AddMoveActivity.this, "Couldn't save move", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Intent result = new Intent();
                    result.putExtra(RESULT_MOVE, newMove);
                    result.putExtra(RESULT_PARENT_NAME, parentName);
                    setResult(Activity.RESULT_OK, result);
                    finish();
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        initUI();

        FileUtils.cleanTemp();

        String downloadURL = null;

        MoveCategory category;
        if(getIntent().hasExtra(EXTRA_DOWNLOAD_URL))
        {
            // Started from other app e.g. Youtube
            // In this case, the video should be downloaded immediately
            category = MoveCategory.SIXES;
            downloadURL = getIntent().getStringExtra(EXTRA_DOWNLOAD_URL);
        }
        else
        {
            // Started from ShowMovesActivity
            // In this case, either a move has been duplicated, should be modified,
            // or is to be created from scratch
            category = (MoveCategory)getIntent().getSerializableExtra(EXTRA_TYPE);
        }

        findViews();
        initViews(category);

        setColors(category);
        updateParentSuggestions();

        if(downloadURL == null)
            replaceLastLayoutHistory();
        else
            downloadFileAndStartActivity(downloadURL);
    }

    private void updateParentSuggestions()
    {
        ArrayAdapter<String> suggestionsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line);

        ArrayList<Move> moves;
        switch(moveCategory)
        {
            case SIXES:
                moves = MoveManager.getSixes();
                break;
            case EIGHTS:
                moves = MoveManager.getEights();
                break;
            case CHARLESTON:
                moves = MoveManager.getCharleston();
                break;
            default:
                throw new IllegalStateException("Stop breaking the app");
        }

        ArrayList<String> moveNames = new ArrayList<>(moves.size());
        for(Move move : moves)
            moveNames.add(move.getName());
        suggestionsAdapter.addAll(moveNames);
        parentEditText.setAdapter(suggestionsAdapter);
    }

    private String getYoutubeID(String videoURL)
    {
        int shortIndex = videoURL.indexOf("youtu.be");
        if(shortIndex > -1)
        {
            // ID is after youtu.be and before an (optional) ?
            int questionIndex = videoURL.indexOf('?');

            if(questionIndex == -1)
                return videoURL.substring(shortIndex + "youtu.be/".length());
            else
                return videoURL.substring(shortIndex + "youtu.be/".length(), questionIndex);
        }
        else
        {
            if(!videoURL.contains("youtube.com"))
                return null;

            int vEqualsIndex = videoURL.indexOf("?v=") + 3; // Skip ?v=

            if(vEqualsIndex == -1)
                return null;

            int firstBreakIndex = -1;
            for(int i = vEqualsIndex + 1; i < videoURL.length(); ++i)
            {
                if(videoURL.charAt(i) == '?' || videoURL.charAt(i) == '&')
                {
                    firstBreakIndex = i;
                    break;
                }
            }

            if(firstBreakIndex == -1)
                return videoURL.substring(vEqualsIndex);
            else
                return videoURL.substring(vEqualsIndex, firstBreakIndex);
        }
    }

    private View replaceLastLayout(int layout)
    {
        rootLayout.removeViewAt(rootLayout.getChildCount() - 1);
        return getLayoutInflater().inflate(layout, rootLayout, true);
    }

    private void replaceLastLayoutHistory()
    {
        View historyView = replaceLastLayout(R.layout.activity_addmove_layout_history);

        ArrayList<FileUtils.DownloadedFile> downloadedFiles = FileUtils.getDownloadedFiles();
        ArrayList<String> downloadedFileTitles = new ArrayList<>(downloadedFiles.size());
        for(FileUtils.DownloadedFile downloadedFile : downloadedFiles)
            downloadedFileTitles.add(downloadedFile.title);

        StringList history = (StringList)historyView.findViewById(R.id.addmove_layout_list_strings);
        history.setStrings(downloadedFileTitles);
        history.setOnItemClickListener(new StringList.OnItemClickListener()
        {
            @Override
            public void onItemClick(String text)
            {
                // Start screenshot activity immediately
                screenshotFile(FileUtils.getDownloadedFileByTitle(text).getAbsolutePath());
            }
        });
    }

    private void screenshotFile(String absolutePath)
    {
        Intent screenshotVideoIntent = new Intent(AddMoveActivity.this, VideoScreenshotActivity.class);
        VideoScreenshotActivity.addExtras(screenshotVideoIntent, absolutePath);
        startActivityForResult(screenshotVideoIntent, REQUEST_SCREENSHOT);
    }

    private void downloadFileAndStartActivity(final String videoURL)
    {
        String youtubeID = getYoutubeID(videoURL);
        if(youtubeID == null)
        {
            Toast.makeText(AddMoveActivity.this,
                           "Couldn't get youtube ID from URL",
                           Toast.LENGTH_LONG).show();
            return;
        }

        disableButtons();

        File downloadedFile = FileUtils.getDownloadedFileByName(youtubeID);
        if(downloadedFile != null)
        {
            screenshotFile(downloadedFile.getAbsolutePath());
        }
        else
        {
            View downloadView = replaceLastLayout(R.layout.activity_addmove_layout_download);
            CircleProgressBar videoDownloadingBar = (CircleProgressBar)downloadView.findViewById(R.id.addmove_layout_download_progressbar);
            TextView videoDownloadingText = (TextView)downloadView.findViewById(R.id.addmove_layout_download_text);

            VideoDownloadThread videoDownloadThread = new VideoDownloadThread(videoDownloadingText,
                                                                              videoDownloadingBar,
                                                                              youtubeID);
            videoDownloadThread.setOnDownloadCompleteListener(new VideoDownloadThread.OnDownloadCompleteListener()
            {
                @Override
                public void onDownloadComplete(Boolean success, final File outFile)
                {
                    enableButtons();

                    if(success)
                    {
                        TextView textView = (TextView)findViewById(R.id.addmove_layout_download_text);
                        textView.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                Intent screenshotVideoIntent = new Intent(AddMoveActivity.this,
                                                                          VideoScreenshotActivity.class);
                                VideoScreenshotActivity.addExtras(screenshotVideoIntent,
                                                                  outFile.getAbsolutePath());
                                startActivityForResult(screenshotVideoIntent, REQUEST_SCREENSHOT);
                            }
                        });
                    }
                    else
                    {
                        Toast.makeText(AddMoveActivity.this,
                                       "Couldn't download or find file",
                                       Toast.LENGTH_LONG).show();
                        replaceLastLayoutHistory();
                    }
                }
            });
            videoDownloadThread.execute();
        }
    }

    private void setColors(MoveCategory activityType)
    {
        int color = -1;
        switch(activityType)
        {
            case SIXES:
                color = getColor(R.color.colorSix);
                break;
            case EIGHTS:
                color = getColor(R.color.colorEight);
                break;
            case CHARLESTON:
                color = getColor(R.color.colorCharleston);
                break;
        }

        actionBar.setBackgroundDrawable(new ColorDrawable(color));
    }

    private void disableButtons()
    {
        downloadFromClipboardButton.setEnabled(false);
        downloadButton.setEnabled(false);
        galleryButton.setEnabled(false);

        downloadFromClipboardButton.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
        downloadButton.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
        galleryButton.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
    }

    private void enableButtons()
    {
        downloadFromClipboardButton.setEnabled(true);
        downloadButton.setEnabled(true);
        galleryButton.setEnabled(true);

        downloadFromClipboardButton.getBackground().setColorFilter(null);
        downloadButton.getBackground().setColorFilter(null);
        galleryButton.getBackground().setColorFilter(null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        enableButtons();

        if(resultCode == RESULT_OK)
        {
            if(requestCode == REQUEST_PICK_VIDEO)
            {
                Uri selectedVideoUri = data.getData();
                String videoPath = getPath(selectedVideoUri);

                if(videoPath != null)
                {
                    Intent screenshotVideoIntent = new Intent(AddMoveActivity.this, VideoScreenshotActivity.class);
                    VideoScreenshotActivity.addExtras(screenshotVideoIntent, videoPath);
                    startActivityForResult(screenshotVideoIntent, REQUEST_SCREENSHOT);
                }
            }
            else if(requestCode == REQUEST_SCREENSHOT)
            {
                View galleryRootLayout = replaceLastLayout(R.layout.activity_addmove_layout_gallery);

                ImageGalleryView gallery = (ImageGalleryView)galleryRootLayout.findViewById(R.id.addmove_layout_gallery_gallery);

                ArrayList<File> tempFiles  = FileUtils.getTempFiles();
                for(int i = 0; i < tempFiles.size(); i += 2)
                    gallery.addBitmap(Uri.fromFile(tempFiles.get(i)), Uri.fromFile(tempFiles.get(i + 1)));
            }
        }
        else
        {
            if(requestCode == REQUEST_SCREENSHOT)
                replaceLastLayoutHistory();
        }
    }

    public String getPath(final Uri uri)
    {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if(isKitKat && DocumentsContract.isDocumentUri(this, uri))
        {
            // ExternalStorageProvider
            if(isExternalStorageDocument(uri))
            {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if("primary".equalsIgnoreCase(type))
                {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if(isDownloadsDocument(uri))
            {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(contentUri, null, null);
            }
            // MediaProvider
            else if(isMediaDocument(uri))
            {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if(!"video".equals(type))
                    return null;

                Uri contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if("content".equalsIgnoreCase(uri.getScheme()))
        {
            return getDataColumn(uri, null, null);
        }
        // File
        else if("file".equalsIgnoreCase(uri.getScheme()))
        {
            return uri.getPath();
        }

        return null;
    }

    public String getDataColumn(Uri uri, String selection,
                                       String[] selectionArgs)
    {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try
        {
            cursor = getContentResolver().query(uri, projection, selection, selectionArgs,
                                                null);
            if(cursor != null && cursor.moveToFirst())
            {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        }
        finally
        {
            if(cursor != null)
                cursor.close();
        }
        return null;
    }

    public boolean isExternalStorageDocument(Uri uri)
    {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public boolean isDownloadsDocument(Uri uri)
    {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public boolean isMediaDocument(Uri uri)
    {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
}
