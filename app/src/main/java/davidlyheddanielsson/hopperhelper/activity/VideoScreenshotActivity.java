package davidlyheddanielsson.hopperhelper.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.BoolRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.TreeMap;

import davidlyheddanielsson.hopperhelper.FileUtils;
import davidlyheddanielsson.hopperhelper.R;
import davidlyheddanielsson.hopperhelper.views.imagegalleryview.ImageGalleryView;
import davidlyheddanielsson.hopperhelper.views.imagegalleryview.ImageGalleryViewSave;
import wseemann.media.FFmpegMediaMetadataRetriever;

import net.protyposis.android.mediaplayer.FileSource;
import net.protyposis.android.mediaplayer.MediaPlayer;

import static android.media.MediaMetadataRetriever.OPTION_CLOSEST;

public class VideoScreenshotActivity extends AppCompatActivity implements TextureView.SurfaceTextureListener
{
    private static final int THUMBNAIL_SIZE_TARGET = 256;

    private static final String EXTRA_VIDEO_PATH = "videoscreenshotactivity.url";
    private static final String EXTRA_SCREENSHOT_COUNT = "videoscreenshotactivity.screenshotcount";
    public static void addExtras(Intent intent, String videoPath)
    {
        intent.putExtra(EXTRA_VIDEO_PATH, videoPath);
    }

    public static int getExtra(Intent intent)
    {
        return intent.getIntExtra(EXTRA_SCREENSHOT_COUNT, 0);
    }

    String videoPath;

    ImageButton playPauseButton;
    ImageButton nextButton;
    ImageButton previousButton;
    ImageButton screenshotButton;

    FrameLayout videoParent;
    TextureView videoView;
    MediaPlayer mediaPlayer;
    SeekBar videoSeekBar;

    Handler seekBarUpdaterHandler;
    Runnable seekBarUpdater;
    TextView playbackSpeedText;

    private class Screenshot
    {
        Screenshot(Bitmap bitmap, Bitmap thumbnail)
        {
            this.bitmap = bitmap;
            this.thumbnail = thumbnail;
        }

        Bitmap bitmap;
        Bitmap thumbnail;
    }

    IdentityHashMap<Screenshot, Integer> bitmapIDs = new IdentityHashMap<>();
    int idCounter = 0;

    ImageGalleryViewSave imageGallery;

    float playbackSpeed = 1.0f;
    boolean playAfterSeek = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_screenshot);

        videoPath = getIntent().getStringExtra(EXTRA_VIDEO_PATH);
        if(videoPath.isEmpty())
            throw new IllegalArgumentException("No video path given");

        videoView = (TextureView)findViewById(R.id.videoscreenshot_surfaceview_videosurface);
        videoView.setSurfaceTextureListener(this);

        ImageButton doneButton = (ImageButton)findViewById(R.id.videoscreenshot_button_done);
        ImageButton deleteButton = (ImageButton)findViewById(R.id.videoscreenshot_button_delete);

        doneButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(imageGallery.doneLoading())
                {
                    Intent result = new Intent();
                    result.putExtra(EXTRA_SCREENSHOT_COUNT, imageGallery.getBitmaps().size());
                    setResult(RESULT_OK, result);
                    finish();
                }
                else
                    Toast.makeText(VideoScreenshotActivity.this, "Wait for screenshots to save", Toast.LENGTH_SHORT).show();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        playPauseButton = (ImageButton)findViewById(R.id.videoscreenshot_button_playpause);
        nextButton = (ImageButton)findViewById(R.id.videoscreenshot_button_next);
        previousButton = (ImageButton)findViewById(R.id.videoscreenshot_button_previous);
        screenshotButton = (ImageButton)findViewById(R.id.videoscreenshot_button_screenshot);
        videoSeekBar = (SeekBar)findViewById(R.id.videoscreenshot_seekbar_seeker);
        playbackSpeedText = (TextView)findViewById(R.id.videoscreenshot_text_playbackspeed);

        imageGallery = (ImageGalleryViewSave)findViewById(R.id.videoscreenshot_imagegallery);
        imageGallery.setFragmentManager(getSupportFragmentManager());
        imageGallery.setOnImageRemovedListener(new ImageGalleryViewSave.OnImageRemovedListener()
        {
            @Override
            public void onImageRemoved(int clickedImage)
            {
                ArrayList<File> tempFiles = FileUtils.getTempFiles();

                FileUtils.deleteFromTempFiles(tempFiles.get(clickedImage * 2)); // Remove thumbnail
                FileUtils.deleteFromTempFiles(tempFiles.get(clickedImage * 2)); // Remove bitmap
            }
        });

        playPauseButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mediaPlayer.isPlaying())
                    pause();
                else
                    play();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(playbackSpeed == 0.1f)
                    playbackSpeed = 0.25f;
                else
                {
                    playbackSpeed += 0.25f;
                    if(playbackSpeed > 1.0f)
                        playbackSpeed = 1.0f;
                }

                updatePlaybackSpeed();
            }
        });

        nextButton.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                playbackSpeed = 1.0f;
                updatePlaybackSpeed();

                return true;
            }
        });

        previousButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                playbackSpeed -= 0.25f;
                if(playbackSpeed < 0.25f)
                    playbackSpeed = 0.1f;

                updatePlaybackSpeed();
            }
        });

        previousButton.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                playbackSpeed = 0.1f;
                updatePlaybackSpeed();

                return true;
            }
        });

        videoSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
            {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {
                if(mediaPlayer.isPlaying())
                {
                    playAfterSeek = true;
                    pause();
                }
                else
                    playAfterSeek = false;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {
                mediaPlayer.seekTo(seekBar.getProgress());

                if(playAfterSeek)
                    play();

            }
        });

        screenshotButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                screenshotVideo();
            }
        });

        videoParent = (FrameLayout)findViewById(R.id.videoscreenshot_frame_videoframe);
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener()
        {
            @Override
            public void onSeekComplete(MediaPlayer mp)
            {
                videoSeekBar.setProgress(mp.getCurrentPosition());

            }
        });

        seekBarUpdaterHandler = new Handler();
        seekBarUpdater = new Runnable()
        {
            @Override
            public void run()
            {
                int currentPosition = mediaPlayer.getCurrentPosition();
                videoSeekBar.setProgress(currentPosition);

                seekBarUpdaterHandler.postDelayed(this, 100);
            }
        };
    }

    private void screenshotVideo()
    {
        class SaveToDiskClass extends AsyncTask<Void, Void, Boolean>
        {
            private Bitmap screenshot;
            private Bitmap thumbnail;
            private int id;
            private int imageGalleryID;

            private File outScreenshot;
            private File outThumbnail = null; // This must be initialized to null

            SaveToDiskClass(Bitmap screenshot, Bitmap thumbnail, int id, int imageGalleryID)
            {
                this.screenshot = screenshot;
                this.thumbnail = thumbnail;
                this.id = id;
                this.imageGalleryID = imageGalleryID;
            }

            @Override
            protected Boolean doInBackground(Void... params)
            {
                outScreenshot = FileUtils.saveTempWithoutAddingToList(screenshot, id + ".png");

                // TODO: Cleanup if fail?
                if(outScreenshot != null)
                    outThumbnail = FileUtils.saveTempWithoutAddingToList(thumbnail, id + "_t.png");

                return outThumbnail != null;
            }

            @Override
            protected void onPostExecute(Boolean success)
            {
                if(success)
                {
                    FileUtils.addToTempFiles(outScreenshot);
                    FileUtils.addToTempFiles(outThumbnail);

                    imageGallery.doneSaving(imageGalleryID);
                }
            }
        }

        AsyncTask<Void, Void, Screenshot> screenshotVideo = new AsyncTask<Void, Void, Screenshot>()
        {
            int seekMS;
            int imageGalleryID;

            @Override
            protected void onPreExecute()
            {
                imageGalleryID = imageGallery.addLoading();
                seekMS = videoSeekBar.getProgress() * 1000;
            }

            @Override
            protected Screenshot doInBackground(Void... params)
            {
                FFmpegMediaMetadataRetriever retriever = new FFmpegMediaMetadataRetriever();
                retriever.setDataSource(videoPath);
                Bitmap bitmap = retriever.getFrameAtTime(seekMS, OPTION_CLOSEST);

                Size thumbnailSize = getScaledSize(bitmap.getWidth(), bitmap.getHeight(), THUMBNAIL_SIZE_TARGET);
                Bitmap thumbnail = Bitmap.createScaledBitmap(bitmap, thumbnailSize.width, thumbnailSize.height, true);

                return new Screenshot(bitmap, thumbnail);
            }

            @Override
            protected void onPostExecute(Screenshot screenshot)
            {
                imageGallery.doneLoading(imageGalleryID, screenshot.thumbnail, screenshot.bitmap);

                int id = idCounter++;
                bitmapIDs.put(screenshot, id);

                new SaveToDiskClass(screenshot.bitmap, screenshot.thumbnail, id, imageGalleryID).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        };
        screenshotVideo.execute();
    }

    private void updatePlaybackSpeed()
    {
        DecimalFormat format = new DecimalFormat("x#.##");
        format.setMinimumFractionDigits(1);
        playbackSpeedText.setText(format.format(playbackSpeed));

        if(mediaPlayer.isPlaying())
            mediaPlayer.setPlaybackSpeed(playbackSpeed);
    }

    private void play()
    {
        if(!mediaPlayer.isPlaying())
        {
            mediaPlayer.setPlaybackSpeed(playbackSpeed);

            playPauseButton.setImageResource(R.drawable.ic_pause);
            mediaPlayer.start();
            runOnUiThread(seekBarUpdater);
        }
    }

    private void pause()
    {
        if(mediaPlayer.isPlaying())
        {
            playPauseButton.setImageResource(R.drawable.ic_play);

            mediaPlayer.pause();
            seekBarUpdaterHandler.removeCallbacks(seekBarUpdater);
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height)
    {
        Surface videoSurface = new Surface(surface);

        try
        {
            mediaPlayer.setDataSource(new FileSource(new File(videoPath)));
            mediaPlayer.setSurface(videoSurface);

            mediaPlayer.prepare();
        }
        catch(IOException e)
        {
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }
        videoSeekBar.setMax(mediaPlayer.getDuration());

        int videoWidth = mediaPlayer.getVideoWidth();
        int videoHeight = mediaPlayer.getVideoHeight();

        int maxWidth = videoParent.getWidth();
        int maxHeight = videoParent.getHeight();

        Size finalSize = getScaledSize(videoWidth, videoHeight, maxWidth, maxHeight);

        ViewGroup.LayoutParams params = videoView.getLayoutParams();
        params.width = finalSize.width;
        params.height = finalSize.height;
        videoView.setLayoutParams(params);

        mediaPlayer.seekTo(0);
    }

    private class Size
    {
        int width;
        int height;
    }

    private Size getScaledSize(int width, int height, int maxWidth, int maxHeight)
    {
        Size finalSize = new Size();

        float aspectRatio = width / (float)height;

        if(maxWidth > maxHeight)
        {
            // Fit left/right
            finalSize.width = maxWidth;
            finalSize.height = Math.round(maxWidth / aspectRatio);
        }
        else
        {
            // Fit top/bottom
            finalSize.width = Math.round(maxHeight * aspectRatio);
            finalSize.height = maxHeight;
        }

        return finalSize;
    }

    private Size getScaledSize(int width, int height, int maxSize)
    {
        Size finalSize = new Size();

        float aspectRatio = width / (float)height;

        if(width > height)
        {
            // Fit left/right
            finalSize.width = maxSize;
            finalSize.height = Math.round(maxSize / aspectRatio);
        }
        else
        {
            // Fit top/bottom
            finalSize.width = Math.round(maxSize * aspectRatio);
            finalSize.height = maxSize;
        }

        return finalSize;
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height)
    {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface)
    {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface)
    {

    }

}
